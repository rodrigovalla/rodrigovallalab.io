let s, m, w, h;
let grid;
let bg, c, ac, cs;
let polysize;
let clicks;
let icon;

function preload() {
	icon = loadImage("../assets/img/polyominosicon.png");
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	let config = getURLParams();
	frameRate(24);
	startConfig(config);
	background(bg);
	noStroke();
	noLoop();
}

function draw() {
	background(bg);
	for (let i = 0; i < h; i++){
		for (let j = 0; j < w; j++){
			if (states[i][j] === 1) {
				fill(cs[i][j]);
			} else {
				fill(bg);
			}
			rect(4+m+(s+m)*j,4+m+(s+m)*i,s,s,10);
		}
	}
	image(icon, 4+m+(s+m)*(w-1), 4+m+(s+m)*(h-1), s, s);
}

function processEv(){
	let x = floor(mouseX / (s + m));
	let y = floor(mouseY / (s + m));
	try {
		states[y][x] = (states[y][x] + 1) % 2;
		if (states[y][x] === 1) {
			checkColor(clicks);
			cs[y][x] = c[ac];
			clicks = (clicks + 1) % polysize;
		} else {
			clicks -= 1;
		}
	} catch (e) {
		print("Click is out of the matrix...");
	}
	redraw();
	return false;
}

function checkColor(clicks){
	if (clicks % polysize === 0) {
		ac = (ac + 1) % polysize;
	}
}

function startConfig(config){
	let number = Number(config.polyominos_size);
	if (typeof(number) === "number" && Number.isInteger(number) && number < 9) {
    	polysize = number;
	} else {
    	polysize = 4;
	}
	number = Number(config.grid);
	let grid_size
	if (typeof(number) === "number" && Number.isInteger(number)) {
    	grid_size = number;
	} else {
    	grid_size = 16;
	}
	let band;
	if (width > height){
		band = width / grid_size;
		w = grid_size;
		m = band * 0.05;
		s = band * 0.94;
		h = floor(height / band);
	} else {
		band = height / grid_size;
		h = grid_size;
		m = band * 0.05;
		s = band * 0.94;
		w = floor(width / band);
	}
	bg = color(20);
	clicks = 0;
	ac = 0;
	c = [];
	c.push(color(255,236,57));
	c.push(color(61,104,250));
	c.push(color(250,60,60));
	c.push(color(60,250,100));
	c.push(color(252,184,61));
	c.push(color(200,200,200));
	c.push(color(200,50,200));
	c.push(color(50,210,230));
	states = [];
	cs = [];
	for (let i = 0; i < h; i++){
		let aux = [];
		let auxc = [];
		for (let j = 0; j < w; j++){
			aux[j] = 0;
			auxc.push(color(20));
		}
		states[i] = aux;
		cs[i] = auxc;
	}
}
