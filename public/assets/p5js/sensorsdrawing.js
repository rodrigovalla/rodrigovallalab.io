let scaleX, scaleY;
let x, y;
let bg, c;
let wsURL;
let wsPath;
let socket;

function setup() {
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	let config = getURLParams();
	frameRate(24);
	startConfig(config);
	background(bg);
	noLoop();
	socket = io.connect(wsURL, {path: wsPath});
	socket.on("message", data => {
		print("epa");
	});
}

function newPoint(x, y, z) {
	print("ojota");
	strokeWeight(z);
	stroke(c[0])
	point(x,y);
}

function processEv(){
	background(bg);
	return false;
}

function startConfig(config){
	bg = color(0,0,0);
	c = [];
	c.push(color(255,236,57));
	c.push(color(61,104,250));
	c.push(color(250,60,60));
	c.push(color(60,250,100));
	c.push(color(252,184,61));
	c.push(color(200,200,200));
	c.push(color(200,50,200));
	c.push(color(50,210,230));
	wsURL = getWSURL(config.ip, config.port);
	wsPath = getWSPath(config.sensor);
}

function getWSURL(ip, port){
	let wsURL = "ws://";
	wsURL += ip;
	wsURL += ":" + port;
	return wsURL;
}

function getWSPath(sensor){
	let wsSensor = "/sensor/connect?type=" + sensor;
	return wsSensor;
}
