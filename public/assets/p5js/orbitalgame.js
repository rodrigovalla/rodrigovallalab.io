let state, speedset, setx, sety, ap;
let title, plabel, llabel, championship, texts, champs;
let levels, level, score;
let lastclick;
let cx, cy, bco, bct;
let g, gs, b;

function setup() {
	getAudioContext().suspend();
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	strokeWeight(5);
	frameRate(50);
	let config = getURLParams();
	startConfig(config);
	background(bco);
	g = new gravity(levels[level - 1][0], levels[level - 1][1], levels[level - 1][2], false);
	ap = g.getAsteroidPosition(0);
	print("orbital game: v0.90");
}

function draw() {
	ap = g.getAsteroidPosition(0);
	switch (state) {
		case 0:
			preFrame();
			break;
		case 1:
			gameFrame();
			break;
		case 2:
			g.display();
			printChampionship();
			noLoop();
			break;
		case -1:
			background(bct);
			g.display();
			b.display(mouseX, mouseY);
			break;
	}
	fill(bco);
	rect(width - 170, height - 120, 170, 120);
	fill(255);
	textAlign(LEFT, BOTTOM);
	textFont("Georgia");
	textSize(22);
	text(title, width - 150, height - 80);
	text(llabel + str(level), width - 150, height - 50);
	text(plabel + str(round(score)), width - 150, height - 20);
}

function preFrame() {
	background(bco);
	stroke(255,0,0,255);
	if (speedset === false) {
		setx = mouseX;
		sety = mouseY;
	}
	line(ap.x, ap.y, setx, sety);
	noStroke();
	g.display();
	b.display(mouseX, mouseY);
}

function gameFrame() {
	background(bct);
	g.display();
	score += p5.Vector.sub(ap, this.C).mag() / 200;
	checkState();
	if (frameCount%6 === 0) {
		gs.updateDopplerFactor(ap, g.C);
		gs.updateSin();
	}
}

function processEv() {
    switch (state) {
    	case 0:
			if (b.contains(mouseX, mouseY)) {
				play();
				lastclick = millis();
			} else {
				setx = mouseX;
				sety = mouseY;
				setSpeed(setx, sety);
			}
       		break;
		case -1:
			userStartAudio();
			gs = new gamesounds();
			state = 0;
			break;
    }
}

function play() {
	g.play();
	gs.playDoppler();
	background(bco);
	score = 0;
	state = 1;
	speedset = false;
}

function setSpeed(x, y) {
	g.setAsteroidsSpeed(x, y);
	speedset = true;
}

function checkState() {
	if (g.isAsteroidInCanvas(0) === false) {
		gs.playLose();
		g.stop();
		g.reset();
		gs.stopDoppler();
		state = 0;
	} else if (millis() - lastclick > 12000) {
		if (level < 8) {
			level += 1;
			gs.playWin();
			g.stop();
			gs.stopDoppler();
			g = new gravity(levels[level - 1][0], levels[level - 1][1], levels[level - 1][2], false);
			state = 0;
		} else {
			g.stop();
			gs.stopDoppler();
			gs.playWin();
			state = 2;
		}

	}
}

function printChampionship() {
	fill(bco);
	ellipse(cx, cy, cx / 2, cx / 2);
	fill(255);
	textFont("Georgia");
	textSize(cx / 5);
	textAlign(CENTER, CENTER);
	text(championship, width / 2, height / 2);
}

function startConfig(config) {
	b = new button(65, height - 65, 35, color(250,60,30), color(125,30,15), "", color(255));
	cx = width / 2;
	cy = height / 2;
	lastclick = 0;
	score = 0;
	state = -1;
	speedset = false;
	setx = 0;
	sety = 0;
	levels = [[1,1,15], [1,1,25], [1,1,35], [2,1,20], [2,1,25], [3,2,20], [3,5,25], [5,10,15]];
	bco = color(15,15,30,255);
	bct = color(15,15,30,10);
	let number = Number(config.level);
	if (typeof(number) === "number" && Number.isInteger(number) && number < 9) {
		level = number;
	} else {
    	level = 1;
  	}
  	let string = config.lan;
  	if (typeof string === "string" && string === "eng") {
    	title = "Orbital Game";
		plabel = "Score: ";
		llabel = "Level: ";
		championship = "You are the champion!";
  	} else {
		title = "Juego orbital";
		plabel = "Puntaje: ";
		llabel = "Nivel: ";
		championship = "¡Sos invencible!";
	}
	noStroke();
}
