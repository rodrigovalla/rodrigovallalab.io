let c, lc, bg, lastColor, lw;

function setup() {
	createCanvas(windowWidth, windowHeight);
	let config = getURLParams();
	startConfig(config);
	stroke(c[lc]);
	strokeWeight(lw);
	background(bg);
	print("This is only a blackboard.");
}

function draw(){
	if (mouseIsPressed === true){
		line(pmouseX,pmouseY,mouseX,mouseY);
	}
}

function keyPressed(){
	if (key === "e"){
		background(bg);
	} else if (key === "+"){
		lw += 1;
		strokeWeight(lw);
	} else if (key === "-"){
		lw -= 1;
		strokeWeight(lw);
	} else if (key === "c"){
		lc = (lc + 1)%c.length;
		stroke(c[lc]);
	}
}

function startConfig(config){
	let string = config.background;
	bg = color(45);
	if (typeof(string) === "string"){
		values = string.split(",");
		if (values.length === 3){
			rgb = [Number(values[0]), Number(values[1]), Number(values[2])]
			if (!(isNaN(rgb[0]) || isNaN(rgb[1] || isNaN(rgb[2])))){
				bg = color(rgb[0], rgb[1], rgb[2]);
			}
		}
	}
	let number = Number(config.width);
  	if (typeof(number) === "number" && Number.isInteger(number)) {
    	lw = number;
	} else {
		lw = 3;
	}
	c = []
	c.push(color(200));
	c.push(color(200,50,50));
	c.push(color(50,200,50));
	c.push(color(50,100,200));
	c.push(color(20));
	c.push(bg);
	//{color(200),color(200,50,50),color(50,200,50),color(50,50,200),color(0)};
	lc = 0;
}
