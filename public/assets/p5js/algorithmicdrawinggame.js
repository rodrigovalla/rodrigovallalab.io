let lw, step, textline;
let level_color, user_color, bg;
let userpoints, levels, levelscount, thelevel;
let thecanvas, showinfo;
let origin, lastpoint;
let currentiterations, currentsize, direction, currentdirection, wheel;
let level_size, level_dna, level_sizes, level_directions, level_iterations;
let user_dna, user_sizes, user_directions, user_iterations;

function preload() {
	fontb = loadFont("../assets/fonts/Comfortaa_Bold.ttf");
	levels = loadTable("../assets/data/algorithmicdrawinggamelevels.csv");
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	config = getURLParams();
	startConfig(config);
	noLoop();
	print("Algorithmic drawing game v0.6");
	getScreen();
}

function getScreen(){
	background(bg);
	if (showinfo){
		printInfo();
	}
	buildDrawing(level_color, level_dna, level_sizes, level_directions, level_iterations);
	buildDrawing(user_color, user_dna, user_sizes, user_directions, user_iterations);
	print("ADN: " + user_dna);
	print("Direcciones: " + user_directions);
	print("Tamaños: " + user_sizes);
	print("Repeticiones: " + user_iterations);
}

function printInfo(){
	noStroke();
	fill(user_color);
	text("Nivel: " + str(thelevel + 1), 25, 25);
	text("Etapas: " + str(level_size), 25, 25 + textline);
	text("Puntos: " + str(userpoints), 25, 25 + textline * 2);
	text("Repeticiones: " + str(currentiterations), 25, 25 + textline * 4);
	text("Dirección: " + str(direction), 25, 25 + textline * 5);
	text("Tamaño: " + str(currentsize), 25, 25 + textline * 6);	
}

function buildDrawing(c, dna, sizes, directions, iterations){
	lastpoint = origin;
	for (let i = 0; i < dna.length; i++){
		let b = dna[i];
		let l = Number(sizes[i]);
		currentdirection = Number(directions[i]);
		let r = Number(iterations[i]);
		for (let j = 0; j < r; j++){
			switch (b) {
				case "D":
					drawing(c, l);
					break;
				case "S":
					aStep(c, l);
					break;
				case "P":
					aPike(c, l);
					break;
				case "T":
					aTooth(c, l);
					break;
				case "R":
					aRoller(c, l);
					break;
				case "J":
					aJump(l);
					break;
				case "4":
					aSquare(c, l);
					break;
				case "8":
					anOctagon(c, l);
					break;
			}
		}
	}
}

function getPoint(p, d, l){
	p2 = [];
	p2[0] = p[0] + wheel[d][0] * step * l;
	p2[1] = p[1] + wheel[d][1] * step * l;
	return p2;
}

function drawing(c, l){
	stroke(c);
	p2 = getPoint(lastpoint, currentdirection, l);
	line(lastpoint[0],lastpoint[1],p2[0],p2[1]);
	lastpoint = p2;
}

function aJump(l){
	p2 = getPoint(lastpoint, currentdirection, l);
	lastpoint = p2;
}

function turn(t){
	currentdirection = (currentdirection + t)%8;
}

function aStep(c, l){
	drawing(c, l);
	turn(2);
	drawing(c, l);
	turn(6);
}

function aPike(c, l){
	drawing(c, l);
	turn(7);
	drawing(c, l);
	turn(2);
	drawing(c, l);
	turn(7);
	drawing(c, l);
}

function aTooth(c, l){
	drawing(c, l);
	turn(3);
	drawing(c, l);
	turn(5);
}

function aRoller(c, l){
	drawing(c, l*2);
	turn(6);
	drawing(c, l);
	turn(6);
	drawing(c, l);
	turn(6);
	drawing(c, l*2);
	turn(6);
}

function aSquare(c, l){
	drawing(c, l);
	turn(2);
}

function anOctagon(c, l){
	drawing(c, l);
	turn(1);
}

function keyPressed() {
	if (key === "D" || key === "d") {
		user_dna += "D";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "S" || key === "s") {
		user_dna += "S";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "P" || key === "p") {
		user_dna += "P";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "T" || key === "t") {
		user_dna += "T";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "R" || key === "r") {
		user_dna += "R";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "J" || key === "j") {
		user_dna += "J";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "4") {
		user_dna += "4";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "8") {
		user_dna += "8";
		user_sizes += str(currentsize);
		user_directions += str(direction);
		user_iterations += str(currentiterations);
	} else if (key === "+") {
		currentsize = (currentsize + 1)%10;
	} else if (key === "-") {
		let m = (currentsize - 1)%10;
		if (m < 0){
			m += 10;
		}
		currentsize = m;
	} else if (keyCode === UP_ARROW) {
		currentiterations = (currentiterations + 1)%10;
	} else if (keyCode === DOWN_ARROW) {
		let m = (currentiterations - 1)%8;
		if (m < 0){
			m += 8;
		}
		currentiterations = m;
	} else if (keyCode === RIGHT_ARROW) {
		direction = (direction + 1)%8;
	} else if (keyCode === LEFT_ARROW) {
		let m = (direction - 1)%8;
		if (m < 0){
			m += 8;
		}
		direction = m;
	} else if (keyCode === BACKSPACE){
		l = user_dna.length - 1;
		user_dna = user_dna.substring(0, l);
		user_sizes = user_sizes.substring(0, l);
		user_directions = user_directions.substring(0, l);
		user_iterations = user_iterations.substring(0, l);
	}  else if (keyCode === DELETE){
		user_dna = "";
		user_sizes = "";
		user_directions = "";
		user_iterations = "";
	} else if (keyCode === ENTER){
		saveCanvas("algorithmicdrawing.jpg")
	}
	userpoints -= 1;
	getScreen();
	if (checkDraw()){
		loadNewLevel();
		setTimeout(getScreen, 500);
	}
}

function processEv(){
	origin = [mouseX, mouseY];
	getScreen();
}

function checkDraw(){
	let success = false;
	if (user_dna === level_dna){
		if (user_directions === level_directions){
			if (user_sizes === level_sizes){
				if (user_iterations === level_iterations){
					success = true;
				}
			}
		}
	}
	return success;
}

function loadNewLevel(){
	userpoints += 100;
	thelevel = (thelevel + 1)%levelscount;
	level_dna = levels.getString(thelevel + 1, 1);
	level_size = level_dna.length;
	level_sizes = levels.getString(thelevel + 1, 2);
	level_directions = levels.getString(thelevel + 1, 3);
	level_iterations = levels.getString(thelevel + 1, 4);
	user_dna = "";
	user_sizes = "";
	user_directions = "";
	user_iterations = "";
}

function startConfig(config) {
	origin = [width * 0.33, height * 0.66];
	lastpoint = origin;
	currentdirection = 0;
	direction = 0;
	currentiterations = 1;
	wheel = [[0,-1],[1,-1],[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1]];
	user_dna = "";
	user_sizes = "";
	user_directions = "";
	user_iterations = "";
	let number = Number(config.level);
  	if (typeof(number) === "number" && Number.isInteger(number)) {
    	thelevel = number - 2;
	} else {
		thelevel = -1;
	}
	userpoints = 0;
	levelscount = levels.getRowCount() - 1;
	loadNewLevel();
	number = Number(config.step);
  	if (typeof(number) === "number" && Number.isInteger(number)) {
    	step = number;
	} else {
		step = 25;
	}
	number = Number(config.lw);
	if (typeof(number) === "number" && Number.isInteger(number)) {
    	lw = number;
	} else {
		lw = 3;
	}
	let string = config.size;
	if (typeof(string) === "string" && Number.isInteger(Number(string))) {
		currentsize = Number(number);
	} else {
		currentsize = 1;
	}
	string = config.info;
	if (typeof(string) === "string" && string === "false") {
    	showinfo = false;
	} else {
		showinfo = true;
	}
	string = config.background;
	bg = color(34);
	if (typeof(string) === "string"){
		values = string.split(",");
		print(values);
		if (values.length === 3){
			rgb = [Number(values[0]), Number(values[1]), Number(values[2])]
			print(rgb);
			if (!(isNaN(rgb[0]) || isNaN(rgb[1] || isNaN(rgb[2])))){
				bg = color(rgb[0], rgb[1], rgb[2]);
			}
		}
	}
	string = config.usercolor;
	user_color = color(153,170,169);
	if (typeof(string) === "string"){
		values = string.split(",");
		print(values);
		if (values.length === 3){
			rgb = [Number(values[0]), Number(values[1]), Number(values[2])]
			if (!(isNaN(rgb[0]) || isNaN(rgb[1] || isNaN(rgb[2])))){
				user_color = color(rgb[0], rgb[1], rgb[2]);
			}
		}
	}
	string = config.levelcolor;
	level_color = color(75,75,75);
	if (typeof(string) === "string"){
		values = string.split(",");
		print(values);
		if (values.length === 3){
			rgb = [Number(values[0]), Number(values[1]), Number(values[2])]
			if (!(isNaN(rgb[0]) || isNaN(rgb[1] || isNaN(rgb[2])))){
				level_color = color(rgb[0], rgb[1], rgb[2]);
			}
		}
	}
	strokeWeight(lw);
	textAlign(TOP, TOP);
	textFont(fontb);
	textSize(height / 25);
	textline = height / 25 * 1.1;
}
