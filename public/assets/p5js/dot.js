class dot {

  constructor(c, d, x, y, sx, sy){
    this.c = c;
    this.d = d;
    this.x = x;
    this.y = y;
		this.sx = sx;
		this.sy = sy;
  }

  display() {
    fill(this.c);
    noStroke();
    ellipse(this.x, this.y, this.d, this.d);
  }

  move(mx, my) {
    this.x += mx;
    this.y += my;
  }

	accelerate(ax, ay) {
		this.sx += ax;
		this.sy += ay;
	}

	update() {
		this.x += this.sx;
		this.y += this.sy;
	}

  reset(x, y) {
    this.x = x;
    this.y = y;
  }

}
