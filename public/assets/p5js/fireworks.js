let dotscount;
let colors = [];
let s, cx, cy, sx, sy, ax, ay, ww, hh;
let dots = [];
let thecanvas;
let state;

function setup() {
  createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	thecanvas.addEventListener("touchstart", processEv, false);
	let config = getURLParams();
  startConfig(config);
}

function draw() {
	if (state === 1) {
		for (let d = 0; d < dotscount; d++){
			dots[d].accelerate(ax, ay);
			dots[d].update();
			dots[d].display();
		}
	}
}

function resetDots(){
  for (let d = 0; d < dotscount; d++){
    dots[d].reset(cx, cy);
  }
}

function processEv(){
	switch (state) {
		case 0:
			state = 1;
			break;
		case 1:
			state = 0;
			saveCanvas("Fireworks", "png");
			break;
	}
  return false;
}

function getColor(){
  let c = random();
  if (c < 0.5){
    return colors[1];
  } else {
    return colors[2];
  }
}

function startConfig(config) {
	state = 0;
	ww = width / 2;
	hh = height / 2;
  let number = Number(config.size);
  if (typeof(number) === "number" && Number.isInteger(number)) {
    s = number;
  } else {
    s = 15;
  }
	number = Number(config.dots);
  if (typeof(number) === "number" && Number.isInteger(number)) {
    dotscount = number;
  } else {
    dotscount = 50;
  }
	number = Number(config.cx);
  if (typeof(number) === "number" && Number.isInteger(number)) {
    cx = number;
  } else {
    cx = width / 2;
  }
	number = Number(config.cy);
  if (typeof(number) === "number" && Number.isInteger(number)) {
    cy = number;
  } else {
    cy = height / 2;
  }
	number = Number(config.sx);
  if (typeof(number) === "number" && Number.isFinite(number)) {
    sx = number;
  } else {
    sx = 6;
  }
	number = Number(config.sy);
  if (typeof(number) === "number" && Number.isFinite(number)) {
		sy = number;
  } else {
    sy = 6;
  }
	number = Number(config.ax);
  if (typeof(number) === "number" && Number.isFinite(number)) {
    ax = number;
  } else {
    ax = 0.0;
  }
	number = Number(config.ay);
  if (typeof(number) === "number" && Number.isFinite(number)) {
    ay = number;
  } else {
    ay = 0.01;
  }
	let aux = random([0,1,2]);
	let auxcolors = [];
	auxcolors[0] = color(250,205,0);
	auxcolors[1] = color(36,180,250);
	auxcolors[2] = color(250, 5, 180);
	for (let c = 0; c < 3; c++) {
		colors[c] = color(auxcolors[(c + aux)%3]);
	}
	for (let d = 0; d < dotscount; d++){
		let ssx = (random(sx) - sx / 2);
		let ssy = (random(sy) - sy / 2);
    dots.push(new dot(getColor(), s, ww + cx, hh + cy, ssx, ssy));
  }
	background(colors[0]);
}
