let s, m, w, h;
let grid;
let bg, alphabg, c, ac, cs;
let colorCount;
let b;
let icon;

function preload() {
	let name = "dith_";
	if (windowWidth > windowHeight) {
		name += "h_";
		name += round(random(0,8));
		name += ".jpg";
	} else {
		name += "v_";
		name += round(random(0,5));
		name += ".jpg";
	}
	bgImg = loadImage("../assets/img/manualdithering/" + name);
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	let config = getURLParams();
	frameRate(24);
	startConfig(config);
	background(bg);
	noLoop();
}

function draw() {
	background(bg);
	noStroke();
	image(bgImg, 0, 0, width, height);
	for (let i = 0; i < h; i++){
		for (let j = 0; j < w; j++){
			if (states[i][j] === 1) {
				fill(cs[i][j]);
			} else {
				fill(alphabg);
			}
			rect(4+m+(s+m)*j,4+m+(s+m)*i,s,s,10);
		}
	}
	b.display(mouseX, mouseY);
}

function processEv(){
	let x = floor(mouseX / (s + m));
	let y = floor(mouseY / (s + m));
	if (b.contains(mouseX, mouseY) && x === 0 & y === 0){
		ac = (ac + 1) % colorCount;
		b.setColors(c[ac], c[ac], c[ac]);
	} else {
		try {
			states[y][x] = (states[y][x] + 1) % 2;
			if (states[y][x] === 1) {
				cs[y][x] = c[ac];
			}
		} catch (e) {
			print("Click is out of the matrix...");
		}
	}
	redraw();
  return false;
}

function startConfig(config){
	let number = Number(config.colors);
  if (typeof(number) === "number" && Number.isInteger(number) && number < 9) {
    colorCount = number;
  } else {
    colorCount = 4;
  }
	number = Number(config.grid);
	let grid_size
	if (typeof(number) === "number" && Number.isInteger(number)) {
    grid_size = number;
  } else {
    grid_size = 32;
  }
	let band;
	if (width > height){
		band = width / grid_size;
		w = grid_size;
		m = band * 0.05;
		s = band * 0.94;
		h = floor(height / band);
	} else {
		band = height / grid_size;
		h = grid_size;
		m = band * 0.05;
		s = band * 0.94;
		w = floor(width / band);
	}
	b = new button(2*m + s/2, 2*m + s/2, s/2, color(255), color(255), "", color(255));
	bg = color(20);
	alphabg = color(0,0,0,0);
	ac = colorCount - 1;
	c = [];
	let step = floor(255/(colorCount-1))
	for (let i = 0; i < colorCount-1; i++){
		c.push(color(i * step))
	}
	c.push(color(255))
	states = [];
	cs = [];
	for (let i = 0; i < h; i++){
		let aux = [];
		let auxc = [];
		for (let j = 0; j < w; j++){
			aux[j] = 0;
			auxc.push(color(20));
		}
		states[i] = aux;
		cs[i] = auxc;
	}
}
